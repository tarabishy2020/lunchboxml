﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using Autodesk.DesignScript.Runtime;
using Autodesk.DesignScript.Geometry;

using ProvingGround.MachineLearning.Classes;

namespace MachineLearning
{
    /// <summary>
    /// Regression Solvers
    /// </summary>
    public static class Regression
    {
        /// <summary>
        /// Linear Regression Solver
        /// </summary>
        /// <param name="TestData">Value to test</param>
        /// <param name="TrainingInputs">Training Inputs</param>
        /// <param name="TrainingOutputs">Training Outputs</param>
        /// <param name="seed">Random seed for solver</param>
        /// <returns name="Result">Predicted result</returns>
        /// <search>regression,machine learning</search>
        [MultiReturn(new[] { "Result" })]
        public static Dictionary<string, object> LinearRegression(double TestData, List<double> TrainingInputs, List<double> TrainingOutputs, int seed)
        {
            double result = clsML.LinearRegression(TestData, TrainingInputs, TrainingOutputs, seed);

            // return a dictionary
            return new Dictionary<string, object>
            {
                {"Result", result}
            };
        }

        /// <summary>
        /// Non Linear Regression Solver
        /// </summary>
        /// <param name="TestData">Value to test</param>
        /// <param name="TrainingInputs">Training Inputs</param>
        /// <param name="TrainingOutputs">Training Outputs</param>
        /// <param name="sigma">Sigma of prediction curve</param>
        /// <param name="complex">Complexity oarameter for the prediction</param>
        /// <param name="seed">Random seed for solver</param>
        /// <returns name="Result">Predicted result</returns>
        /// <returns name="Score">Predicted scores</returns>
        /// <returns name="error">Error between expected and predicted</returns>
        /// <search>regression,machine learning</search>
        [MultiReturn(new[] { "Result", "Score", "error" })]
        public static Dictionary<string, object> NonLinearRegression(List<List<double>> TestData, List<List<double>> TrainingInputs, List<double> TrainingOutputs, double sigma, double complex, int seed)
        {
            Tuple<double[], double[], double> result = clsML.SequentialMinimalOptimizationRegressionModel(TestData, TrainingInputs, TrainingOutputs, sigma, complex, seed);

            // return a dictionary
            return new Dictionary<string, object>
            {
                {"Result", result.Item1},
                {"Score", result.Item2},
                {"error", result.Item3},
            };
        }

        /// <summary>
        /// Logistic Regression Solver
        /// </summary>
        /// <param name="TestData">Value to test</param>
        /// <param name="TrainingInputs">Training Inputs</param>
        /// <param name="TrainingOutputs">Training Outputs</param>
        /// <param name="tolerance">Tolerance value to use to determine of the algorithm has converged.</param>
        /// <param name="maxIter">Maximum number of iterations performed by the algorithm</param>
        /// <param name="regular">Regularization value to be added to the objective function.</param>
        /// <param name="seed">Random seed for solver</param>
        /// <returns name="Result">Predicted result</returns>
        /// <returns name="Score">Predicted scores</returns>
        /// <returns name="error">Error between expected and predicted</returns>
        /// <search>regression,machine learning</search>
        [MultiReturn(new[] { "Result", "Score", "error" })]
        public static Dictionary<string, object> LogisticRegression(List<List<double>> TestData, List<List<double>> TrainingInputs, List<bool> TrainingOutputs, double tolerance, int maxIter, double regular, int seed)
        {
            Tuple<bool[], double[]> result = clsML.LogisticRegression(TestData, TrainingInputs, TrainingOutputs,tolerance, maxIter, regular, seed);

            // return a dictionary
            return new Dictionary<string, object>
            {
                {"Result", result.Item1},
                {"Score", result.Item2},
            };
        }

        /// <summary>
        /// Multivariate Linear Regression Solver
        /// </summary>
        /// <param name="TestData">Value to test</param>
        /// <param name="TrainingInputs">Training Inputs</param>
        /// <param name="TrainingOutputs">Training Outputs</param>
        /// <param name="seed">Random seed for solver</param>
        /// <returns name="Result">Predicted result</returns>
        /// <search>regression,machine learning</search>
        [MultiReturn(new[] { "Result" })]
        public static Dictionary<string, object> MultivariateLinearRegression(List<List<double>> TestData, List<List<double>> TrainingInputs, List<List<double>> TrainingOutputs, int seed)
        {
            double[][] result = clsML.MultivariateLinearRegression(TestData, TrainingInputs, TrainingOutputs, seed);

            // return a dictionary
            return new Dictionary<string, object>
            {
                {"Result", result}
            };
        }
    }

    /// <summary>
    /// Classification Solvers
    /// </summary>
    public static class Classification
    {
        /// <summary>
        /// Naive Bayes Solver
        /// </summary>
        /// <param name="TestData">Value to test</param>
        /// <param name="TrainingData">Training Data Table (use LunchBox Dataset tools)</param>
        /// <param name="InputColumnNames">Names of input columns</param>
        /// <param name="OutputColumnName">Name of the output column</param>
        /// <param name="seed">Random seed for solver</param>
        /// <returns name="Result">Predicted result</returns>
        /// <returns name="Numeric">The numeric output that represents the answer</returns>
        /// <returns name="Probabilities">Probabilities for each possible answer</returns>
        /// <search>classification,machine learning</search>
        [MultiReturn(new[] { "Result", "Numeric", "Probabilities" })]
        public static Dictionary<string, object> NaiveBayesClassifier(List<string> TestInputs, DataTable TrainingData, List<string> InputColumnNames, string OutputColumnName, int seed)
        {
            Tuple<string, int, double[]> result = clsML.NaiveBayesClassifier(TestInputs, TrainingData, InputColumnNames, OutputColumnName, seed);

            // return a dictionary
            return new Dictionary<string, object>
            {
                {"Result", result.Item1},
                {"Numeric", result.Item2},
                {"Probabilities", result.Item3 }
            };
        }

        /// <summary>
        /// Naive Bayes Solver
        /// </summary>
        /// <param name="InputList">The list of training inputs.</param>
        /// <param name="Components">Number of clusters</param>
        /// <param name="seed">Random seed for solver</param>
        /// <returns name="Result">Predicted result</returns>
        /// <returns name="Likelihood">Log-likelyhood that an input belongs to a cluster.</returns>
        /// <returns name="Probabilities">Probabilities for each possible answer</returns>
        /// <search>classification,machine learning</search>
        [MultiReturn(new[] { "Result", "Likelihood", "Probabilities" })]
        public static Dictionary<string, object> GaussianMixture(List<List<double>> InputList, int Components, int seed)
        {
            Tuple<int[], double[], double[][]> result = clsML.GaussianMixture(InputList, Components, seed);

            // return a dictionary
            return new Dictionary<string, object>
            {
                {"Result", result.Item1},
                {"Likelihood", result.Item2},
                {"Probabilities", result.Item3 }
            };
        }
    }

    /// <summary>
    /// Network Solvers
    /// </summary>
    public static class Networks
    {
        /// <summary>
        /// Neural Network Solver
        /// </summary>
        /// <param name="TestData">Value to test</param>
        /// <param name="TrainingData">Training Data Table (use LunchBox Dataset tools)</param>
        /// <param name="LabelList">The list of Labels.</param>
        /// <param name="HiddenNeurons">Number of Hidden Neurons.</param>
        /// <param name="alpha">Sigmoid's alpha value.</param>
        /// <param name="iterations">Number of iterations to teach the network.</param>
        /// <param name="seed">Random seed for solver</param>
        /// <returns name="Result">Predicted result</returns>
        /// <search>networks,machine learning</search>
        [MultiReturn(new[] { "Result" })]
        public static Dictionary<string, object> NeuralNetwork(List<List<double>> TestData, List<List<double>> TrainingData, List<int> LabelList, int HiddenNeurons, double alpha, int iterations, int seed)
        {
            string[] result = clsML.NeuralNetwork(TestData, TrainingData, LabelList, HiddenNeurons, alpha, iterations, seed);

            // return a dictionary
            return new Dictionary<string, object>
            {
                {"Result", result},
            };
        }

        /// <summary>
        /// Restricted Boltzmann Solver
        /// </summary>
        /// <param name="TestData">Value to test</param>
        /// <param name="TrainingInputs">Training input data</param>
        /// <param name="TrainingOutputs">Training output data</param>
        /// <param name="momentum">Momentum term of the learning algorithm.</param>
        /// <param name="decay">Weight decay constant of the learning algorithm.</param>
        /// <param name="learningrate">Learning rate of the learning algorithm.</param>
        /// <param name="iterations">Number of iterations to learn</param>
        /// <param name="seed">Random seed for the solver</param>
        /// <returns name="Result">Predicted result</returns>
        /// <search>networks,machine learning</search>
        [MultiReturn(new[] { "Result" })]
        public static Dictionary<string, object> RestrictedBoltzmann(List<double> TestData, List<List<double>> TrainingInputs, List<List<double>> TrainingOutputs, double momentum, double learningrate, double decay, double alpha, int iterations, int seed)
        {
            double[] result = clsML.RestrictedBoltzmann(TestData, TrainingInputs, TrainingOutputs, momentum, learningrate, decay, alpha, iterations, seed);

            // return a dictionary
            return new Dictionary<string, object>
            {
                {"Result", result},
            };
        }


    }

    /// <summary>
    /// Model solvers
    /// </summary>
    public static class Models
    {
        /// <summary>
        /// Hidden Markov solver
        /// </summary>
        /// <param name="TrainingInputs">The training data to use.</param>
        /// <param name="Generations">Number of generations to produce.</param>
        /// <param name="seed">Random seed for solver</param>
        /// <returns name="Result">Predicted results</returns>
        /// <search>networks,machine learning</search>
        [MultiReturn(new[] { "Result" })]
        public static Dictionary<string, object> HiddenMarkovModel(List<List<string>> TrainingInputs, int Generations, int seed, int states)
        {
            string[] result = clsML.HiddenMarkovModel(TrainingInputs, Generations, seed, states);

            // return a dictionary
            return new Dictionary<string, object>
            {
                {"Result", result},
            };
        }

    }
}
