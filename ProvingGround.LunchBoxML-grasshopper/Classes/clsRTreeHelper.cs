﻿using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Rhino.Collections;
using Rhino.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProvingGround.MachineLearning.Classes
{
    /// <summary>
    /// An RTree helper class
    /// </summary>
    public class clsRTreeHelper
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="clsRTreeHelper"/> class.
        /// </summary>
        public clsRTreeHelper() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="clsRTreeHelper"/> class.
        /// </summary>
        /// <param name="points">The points.</param>
        public clsRTreeHelper(List<Point3d> points)
        {
            Points = new Point3dList();
            Source = enumRTreeSource.Points;
            Tree = new RTree();

            for (int i = 0; i < points.Count; i++)
            {
                Points.Add(points[i]);
                Tree.Insert(points[i], i);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="clsRTreeHelper"/> class.
        /// </summary>
        /// <param name="mesh">The mesh.</param>
        public clsRTreeHelper(Mesh mesh)
        {
            Points = new Point3dList();
            Source = enumRTreeSource.Mesh;
            Tree = new RTree();

            for (int i = 0; i < mesh.Vertices.Count; i++)
            {
                Point3d vertex = new Point3d(mesh.Vertices[i].X, mesh.Vertices[i].Y, mesh.Vertices[i].Z);
                Points.Add(vertex);
                Tree.Insert(vertex, i);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="clsRTreeHelper"/> class.
        /// </summary>
        /// <param name="pointCloud">The point cloud.</param>
        public clsRTreeHelper(PointCloud pointCloud)
        {
            Points = new Point3dList();
            Source = enumRTreeSource.PointCloud;
            Tree = new RTree();

            for (int i = 0; i < pointCloud.Count; i++)
            {
                Point3d point = pointCloud[i].Location;
                Points.Add(point);
                Tree.Insert(point, i);
            }
        }

        /// <summary>
        /// Gets or sets the tree.
        /// </summary>
        /// <value>
        /// The tree.
        /// </value>
        public RTree Tree { get; set; }

        /// <summary>
        /// Gets or sets the points.
        /// </summary>
        /// <value>
        /// The points.
        /// </value>
        public Point3dList Points { get; set; }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public enumRTreeSource Source { get; set; }

        #region Closest point solver

        /// <summary>
        /// Gets the closest point index
        /// </summary>
        /// <param name="searchPoint">The search point.</param>
        /// <returns></returns>
        public int ClosestIndex(Point3d searchPoint)
        {
            var closestPointData = new ClosestPointSearchData(Points);

            this.Tree.Search(new Sphere(searchPoint, searchPoint.DistanceTo(Points[0]) * 1.1), ClosestPointCallback, closestPointData);

            return closestPointData.Index;
        }

        /// <summary>
        /// Callback for getting the closest search point
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RTreeEventArgs"/> instance containing the event data.</param>
        static void ClosestPointCallback(object sender, RTreeEventArgs e)
        {
            var closestPointSearchData = e.Tag as ClosestPointSearchData;

            if (closestPointSearchData == null)
            {
                return;
            }

            Point3d hitPoint = closestPointSearchData.Points[e.Id];

            double distance = hitPoint.DistanceTo(e.SearchSphere.Center);

            if (closestPointSearchData.Index == -1 || distance < closestPointSearchData.Distance)
            {
                e.SearchSphere = new Sphere(e.SearchSphere.Center, distance);
                closestPointSearchData.Distance = distance;
                closestPointSearchData.Index = e.Id;
            }
        }

        #endregion

        #region distance range solver

        /// <summary>
        /// Gets all indices from search point within specified range.
        /// </summary>
        /// <param name="searchPoint">The search point.</param>
        /// <param name="range">The distance.</param>
        /// <returns></returns>
        public List<int> IndicesInRange(Point3d searchPoint, double range)
        {
            var distanceSearchData = new DistanceSearchData();

            this.Tree.Search(new Sphere(searchPoint, range), DistanceCallback, distanceSearchData);

            return distanceSearchData.Ids;
        }

        /// <summary>
        /// Callback for getting points within distance range.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RTreeEventArgs"/> instance containing the event data.</param>
        static void DistanceCallback(object sender, RTreeEventArgs e)
        {
            var distanceSearchData = e.Tag as DistanceSearchData;
            distanceSearchData.Ids.Add(e.Id);
        }

        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    public class ClosestPointSearchData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClosestPointSearchData"/> class.
        /// </summary>
        /// <param name="points">The points.</param>
        public ClosestPointSearchData(Point3dList points)
        {
            Points = points;
            Index = -1;
        }

        /// <summary>
        /// Gets or sets the points.
        /// </summary>
        /// <value>
        /// The points.
        /// </value>
        public Point3dList Points { get; set; }

        /// <summary>
        /// Gets or sets the distance.
        /// </summary>
        /// <value>
        /// The distance.
        /// </value>
        public double Distance { get; set; }

        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public int Index { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DistanceSearchData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DistanceSearchData"/> class.
        /// </summary>
        public DistanceSearchData()
        {
            Ids = new List<int>();
        }

        /// <summary>
        /// Gets or sets the ids.
        /// </summary>
        /// <value>
        /// The ids.
        /// </value>
        public List<int> Ids { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Grasshopper.Kernel.Types.GH_Goo{ProvingGround.MachineLearning.Classes.clsRTreeHelper}" />
    public class GH_clsRTreeHelper : GH_Goo<clsRTreeHelper>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GH_clsRTreeHelper"/> class.
        /// </summary>
        public GH_clsRTreeHelper() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="GH_clsRTreeHelper"/> class.
        /// </summary>
        /// <param name="RTreeHelper">The r tree helper.</param>
        public GH_clsRTreeHelper(clsRTreeHelper RTreeHelper)
        {
            this.Value = RTreeHelper;
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        public override bool IsValid
        {
            get { return this.Value.Points != null; }
        }

        /// <summary>
        /// Gets the name of the type of the implementation.
        /// </summary>
        public override string TypeName
        {
            get { return "RTreeHelper"; }
        }

        /// <summary>
        /// Gets a description of the type of the implementation.
        /// </summary>
        public override string TypeDescription
        {
            get { return "An RTree for fast point proximity tests"; }
        }

        /// <summary>
        /// Make a complete duplicate of this instance. No shallow copies.
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Classes which implement this interface should also provide type specific Duplicate methods
        /// </remarks>
        public override IGH_Goo Duplicate()
        {
            return GH_Convert.ToGoo(this.Value);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "A " + this.Value.Source.ToString() + " based RTree with " + this.Value.Points.Count.ToString() + " points";
        }
    }

    /// <summary>
    /// Source of the RTree
    /// </summary>
    public enum enumRTreeSource
    {
        /// <summary>
        /// The point cloud
        /// </summary>
        PointCloud,
        /// <summary>
        /// The points
        /// </summary>
        Points,
        /// <summary>
        /// The mesh
        /// </summary>
        Mesh
    }
}
