﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using ProvingGround.MachineLearning.Classes;

using System.Text;
using System.Threading.Tasks;

namespace ProvingGround.MachineLearning.Components
{

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Grasshopper.Kernel.GH_Component" />
    public class nodeRTreePointsWithinRange : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="nodeRTreeCreate"/> class.
        /// </summary>
        /// <exclude />
        public nodeRTreePointsWithinRange() : base("RTree Points in Range", "RTree Range", "Find points in an RTree within range of search points", "LunchBox", "Util")
        { }

        /// <summary>
        /// Returns a consistent ID for this object type. Every object must supply a unique and unchanging
        /// ID that is used to identify objects of the same type.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("dbe04b8a-a53f-4eb3-abfb-5866e0bd39d1"); }
        }

        /// <summary>
        /// Gets the exposure of this object in the Graphical User Interface.
        /// The default is to expose everywhere.
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.quarternary; }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.RTree_PtsInRange; }
        }

        /// <summary>
        /// Declare all your input parameters here.
        /// </summary>
        /// <param name="pManager">Use the pManager to register new parameters. pManager is never null.</param>
        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            pManager.AddPointParameter("Search Points", "SPoints", "Points to search from", GH_ParamAccess.tree);
            pManager.AddNumberParameter("Search Distance", "Distance", "Distances to search from", GH_ParamAccess.tree);
            pManager.AddGenericParameter("RTree", "RTree", "The RTree to search", GH_ParamAccess.tree);
        }

        /// <summary>
        /// Declare all your output parameters here.
        /// </summary>
        /// <param name="pManager">Use the pManager to register new parameters. pManager is never null.</param>
        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
            pManager.AddPointParameter("Points within Range", "RPoints", "The points in the RTree within range of search points", GH_ParamAccess.tree);
            pManager.AddIntegerParameter("Indices within Range", "Indices", "The indices of points in the RTree within range of search points", GH_ParamAccess.tree);
        }

        /// <summary>
        /// This function will be called (successively) from within the
        /// ComputeData method of this component.
        /// </summary>
        /// <param name="DA">Data Access object. Use this object to retrieve data
        /// from input parameters and assign data to output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_Structure<GH_Point> searchPoints = new GH_Structure<GH_Point>();
            GH_Structure<GH_Number> searchDistances = new GH_Structure<GH_Number>();
            GH_Structure<IGH_Goo> rTrees = new GH_Structure<IGH_Goo>();

            DA.GetDataTree(0, out searchPoints);
            DA.GetDataTree(1, out searchDistances);
            DA.GetDataTree(2, out rTrees);

            GH_Structure<GH_Integer> closeIndices = new GH_Structure<GH_Integer>();
            GH_Structure<GH_Point> closePoints = new GH_Structure<GH_Point>();

            for (int i = 0; i < searchPoints.PathCount; i++)
            {
                var path = searchPoints.Paths[i];
                var helperBranch = rTrees.Branches[rTrees.PathCount < i ? rTrees.PathCount - 1 : i];

                if (helperBranch.Count == 0)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "No RTree has been supplied that corresponds to branch " + searchPoints.Paths[i].ToString());
                    return;
                }
                else if (helperBranch.Count > 1)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Multiple RTrees have been supplied for branch " + searchPoints.Paths[i].ToString() + ". If you want to search multiple RTrees, locate them on individual branches.");
                    return;
                }

                clsRTreeHelper rTreeHelper = ((GH_clsRTreeHelper)helperBranch[0]).Value;

                var distances = searchDistances.Branches[searchDistances.PathCount > i ? i : searchDistances.PathCount - 1];


                int counter = 0;

                foreach (GH_Point ghPoint in searchPoints.Branches[i])
                {
                    var pointPath = path.AppendElement(counter);
                    var indices = rTreeHelper.IndicesInRange(ghPoint.Value, distances[distances.Count > counter ? counter : distances.Count - 1].Value);

                    if (indices.Count == 0)
                    {
                        closePoints.AppendRange(new List<GH_Point>(), pointPath);
                        closeIndices.AppendRange(new List<GH_Integer>(), pointPath);
                    }
                    else
                    {
                        foreach (int index in indices)
                        {
                            closePoints.Append(new GH_Point(rTreeHelper.Points[index]), pointPath);
                            closeIndices.Append(new GH_Integer(index), pointPath);
                        }
                    }
                    counter += 1;
                }
            }

            DA.SetDataTree(0, closePoints);
            DA.SetDataTree(1, closeIndices);
        }
    }
}