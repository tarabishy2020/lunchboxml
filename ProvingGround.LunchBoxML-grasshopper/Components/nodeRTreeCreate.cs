﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using Rhino.Collections;
using Rhino.Geometry;
using System.Text;
using System.Threading.Tasks;

using ProvingGround.MachineLearning.Classes;

namespace ProvingGround.MachineLearning.Components
{
    /// <summary>
    /// Create the RTree
    /// </summary>
    /// <seealso cref="Grasshopper.Kernel.GH_Component" />
    public class nodeRTreeCreate : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="nodeRTreeCreate"/> class.
        /// </summary>
        /// <exclude />
        public nodeRTreeCreate() : base("Create RTree", "RTree", "Creates a searchable RTree", "LunchBox", "Util")
        { }

        /// <summary>
        /// Returns a consistent ID for this object type. Every object must supply a unique and unchanging
        /// ID that is used to identify objects of the same type.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("25c345c7-9bd6-471a-974c-8fd901adf4b5"); }
        }

        /// <summary>
        /// Gets the exposure of this object in the Graphical User Interface.
        /// The default is to expose everywhere.
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.quarternary; }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.RTree_Create; }
        }


        /// <summary>
        /// Declare all your input parameters here.
        /// </summary>
        /// <param name="pManager">Use the pManager to register new parameters. pManager is never null.</param>
        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Input geometry", "Geo", "Input geometry for creating RTree (List of Points, Mesh, or PointCloud)", GH_ParamAccess.list);
        }

        /// <summary>
        /// Declare all your output parameters here.
        /// </summary>
        /// <param name="pManager">Use the pManager to register new parameters. pManager is never null.</param>
        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("RTree", "RTree", "The solved RTree for rapid search", GH_ParamAccess.list);
        }

        /// <summary>
        /// This function will be called (successively) from within the
        /// ComputeData method of this component.
        /// </summary>
        /// <param name="DA">Data Access object. Use this object to retrieve data
        /// from input parameters and assign data to output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            List<IGH_Goo> inputs = new List<IGH_Goo>();
            DA.GetDataList(0, inputs);

            var treeHelpers = new List<GH_clsRTreeHelper>();

            var points = new List<Point3d>();

            for (int i = 0; i < inputs.Count; i++)
            {
                switch (inputs[i].TypeName)
                {
                    case "Point":
                        Point3d p = Point3d.Unset;
                        inputs[i].CastTo<Point3d>(out p);
                        points.Add(p);
                        break;
                    case "Cloud":
                        PointCloud pc = new PointCloud();
                        inputs[i].CastTo<PointCloud>(out pc);
                        treeHelpers.Add(new GH_clsRTreeHelper(new clsRTreeHelper(pc)));
                        break;
                    case "Mesh":
                        Mesh m = new Mesh();
                        inputs[i].CastTo<Mesh>(out m);
                        treeHelpers.Add(new GH_clsRTreeHelper(new clsRTreeHelper(m)));
                        break;
                    default:
                        AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Unrecognized object type (please supply a list of points, a mesh, or a point cloud object)");
                        return;
                }
            }

            if (points.Count > 0)
            {
                treeHelpers.Add(new GH_clsRTreeHelper(new clsRTreeHelper(points)));
            }

            DA.SetDataList(0, treeHelpers);
        }
    }
}
