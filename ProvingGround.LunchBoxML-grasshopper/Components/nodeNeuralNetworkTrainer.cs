﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Accord.Diagnostics;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using Accord.Neuro.Networks;
using Accord.IO;

using ProvingGround.MachineLearning.Classes;
using System.Windows.Forms;
using System.IO;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Neural Network Node
    /// </summary>
    public class nodeNeuralNetworkTrainer : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public nodeNeuralNetworkTrainer()
            : base("Neural Network Trainer", "Neural Network Trainer", "This component uses the resilient backpropagation (RProp) learning algorithm to train neural networks.", "LunchBox", "Machine Learning")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("cc010201-a5bf-439e-8190-cadb8019b820"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_NeuralNetwork_Trainer; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddNumberParameter("Training Inputs", "Inputs", "The list of training inputs. Assumes values have been remapped to [-1 to 1] domain.", GH_ParamAccess.tree);
            pManager.AddNumberParameter("Training Outputs", "Outputs", "The list of expected outputs. Assumes values have been remapped to [-1 to 1] domain.", GH_ParamAccess.tree);
            pManager.AddIntegerParameter("Input Neurons", "Input Neurons", "Number of Input Neurons.", GH_ParamAccess.item, 2);
            pManager.AddIntegerParameter("Hidden Neurons", "Hidden Neurons", "Number of Hidden Neurons.", GH_ParamAccess.item, 2);
            //pManager.AddNumberParameter("Learning Rate", "Learning Rate", "Learning Rate.", GH_ParamAccess.item, 0.0125);
            pManager.AddNumberParameter("Alpha", "Alpha", "Sigmoid's alpha value.", GH_ParamAccess.item, 2.0);
            pManager.AddIntegerParameter("Iterations", "Iterations", "Number of iterations to teach the network.", GH_ParamAccess.item, 500);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trained Nueral Network", "Neural Network", "Trained Neural Network", GH_ParamAccess.item);
            pManager.AddNumberParameter("Error's Dynamics ", "Error", "Error's Dynamics", GH_ParamAccess.list);
        }
        #endregion

        private void SaveNeuralNetwork_Clicked(object sender, EventArgs e)
        {
            if(network != null)
            {
                System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
                dialog.Title = "Save Neural Network";
                dialog.Filter = "Generic Binary File (*.bin)|*.bin";
                dialog.FilterIndex = 1;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string fullpath = Path.GetFullPath(dialog.FileName);
                    network.Save(fullpath);
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("No valid neural network found. Please create a valid neural network before trying to save.");
            }
            
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            base.AppendAdditionalMenuItems(menu);

            Menu_AppendSeparator(menu);
            Menu_AppendItem(menu, "Save Neural Network", SaveNeuralNetwork_Clicked, Properties.Resources.PG_ML_SaveFile);
        }

        Accord.Neuro.ActivationNetwork network = null;

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // Tree Structure Input Variables         
            GH_Structure<GH_Number> inputs = new GH_Structure<GH_Number>();
            GH_Structure<GH_Number> outputs = new GH_Structure<GH_Number>();

            List<double> errorRate = new List<double>();

            int numOfInputNeurons = 10;
            int numOfHiddenNeurons = 10;
            int numOfIterations = 100;
            //double learningRate = 0.0125;
            double alpha = 2.0;

            //Tree Variables
            if  (
                    DA.GetDataTree<GH_Number>(0, out inputs) &&
                    DA.GetDataTree<GH_Number>(1, out outputs) &&
                    DA.GetData(2, ref numOfInputNeurons) &&
                    DA.GetData(3, ref numOfHiddenNeurons) &&
                    DA.GetData(4, ref alpha) &&
                    DA.GetData(5, ref numOfIterations) 
                )
            {
                // list of lists
                List<List<double>> inputList = new List<List<double>>();
                List<List<double>> outputList = new List<List<double>>();
                List<double> debug = new List<double>();

                /*double minInput = double.MaxValue;
                double maxInput = double.MinValue;
                double minOutput = double.MaxValue;
                double maxOutput = double.MinValue;
                double[] inputRange = { -1.0, 1.0 };
                double[] outputRange = { -1.0, 1.0 };

                // find range of input list
                for (int i = 0; i < inputs.Branches.Count; i++)
                {
                    List<GH_Number> branch = inputs.Branches[i];
                    foreach (GH_Number num in branch)
                    {
                        if (num.Value < minInput) minInput = num.Value;
                        else if (num.Value > maxInput) maxInput = num.Value;
                    }
                }*/

                // add input list with remapped values [-1 to 1]
                for (int i = 0; i < inputs.Branches.Count; i++)
                {
                    List<double> list = new List<double>();
                    List<GH_Number> branch = inputs.Branches[i];
                    foreach (GH_Number num in branch)
                    {
                        list.Add(num.Value);
                        // list.Add((num.Value - minInput) * (inputRange[1] - inputRange[0]) / (maxInput - minInput) + inputRange[0]);
                    }
                    inputList.Add(list);
                }


                // find range of output list
                /* for (int i = 0; i < outputs.Branches.Count; i++)
                {
                    List<GH_Number> branch = outputs.Branches[i];
                    foreach (GH_Number num in branch)
                    {
                        if (num.Value < minOutput) minOutput = num.Value;
                        else if (num.Value > maxOutput) maxOutput = num.Value;
                    }
                }*/

                // add input list with remapped values [-1 to 1]
                for (int i = 0; i < outputs.Branches.Count; i++)
                {
                    List<double> list = new List<double>();
                    List<GH_Number> branch = outputs.Branches[i];
                    foreach (GH_Number num in branch)
                    {
                        list.Add(num.Value);
                        // list.Add((num.Value - minOutput) * (outputRange[1] - outputRange[0]) / (maxOutput - minOutput) + outputRange[0]);
                    }
                    outputList.Add(list);
                }

                //Result
                clsML learning = new Classes.clsML();
                network = learning.BackPropagationTrainer(inputList, outputList, numOfInputNeurons, numOfHiddenNeurons, alpha, numOfIterations, ref errorRate);

                //Output
                DA.SetData(0, network);
                DA.SetDataList(1, errorRate);
            }
        }
        #endregion
    }
}



